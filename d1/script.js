// while loop

// let count = 1;
// while(count < 1){
// 	console.log("count is " + count)
// 	count++
// }

// let num =1;
// let count = 1;
// while(count <= 10){
// 	console.log(num)
// 	num  += 2;

// let num = -9 ;
// while(num <= 19){
//     console.log(num)
//     num++;
// }

// let num = 12;

// while (num < 40){
// 	if(num % 2 == 0){
// 		console.log(num)
// 	}
// 	num ++
// }

// let num = 301;

// while (num < 333){
// 	if(num % 2 !== 0){
// 		console.log(num)
// 	}
// 	num ++
// }

// Do/While Loop

// The do/while loop is a variant of the while loop.
// This loop will execute the code block once, before checking
// if the condition is true, then it will repeat the loop as long as
// the condition is true.

// let i  = 0;
// do{
// 	console.log(i);
// 	i++;
// }
// while (i < 10)

// let num = -9 ;
// do{
// 	console.log(num)
//     num++;	
// 	}
//    while(num <= 19)

// For loop
// it takes in three parameters: step of increment

for(let count= 0; count < 6; count++) {
	console.log(count);
}

